from django.db import models
from django.contrib.auth.models import User

	
class Channel(models.Model):
	name=models.CharField(max_length=50)

	def __unicode__(self):
        	return self.name

class Service(models.Model):
	channel=models.ForeignKey(Channel)
	service_tag=models.CharField(max_length=20)
	service_desc=models.CharField(max_length=100)

	def __unicode__(self):
        	return u'%s %s' %(self.channel.name,self.service_tag)

class Meta:
	unique_together=('channel','service_tag')


class Service_criteria(models.Model):
	service=models.ForeignKey(Service)
	param=models.CharField(max_length=20)

	def __unicode__(self):
        	return self.param

class Mix(models.Model):
	creator=models.ForeignKey(User)
	trigger=models.ForeignKey(Service,related_name='mix_trigger')
	event=models.ForeignKey(Service,related_name='mix_event')
	desc=models.CharField(max_length=140)
	shared=models.BooleanField()

	def __unicode__(self):
        	return u'%s %s %s %s' % (self.trigger.channel.name,self.trigger.service_tag,self.trigger.channel.name,self.event.service_tag)

class Meta:
	unique_together=('creator','trigger','event')

class Owner(models.Model):
	user=models.ForeignKey(User)
	mix=models.ForeignKey(Mix)
	active=models.BooleanField()

	def __unicode__(self):
        	return u'%s %s' % (self.user.username,self.mix.desc)

class Meta:
	unique_together=('user','mix')

class Owner_criteria(models.Model):
	owner=models.ForeignKey(Owner)
	param=models.ForeignKey(Service_criteria)
	val=models.CharField(max_length=50)

	def __unicode__(self):
        	return u'%s %s' % (self.user.username,self.mix.desc)

# Create your models here.
