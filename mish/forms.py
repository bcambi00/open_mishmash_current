from django import forms


class Authorize(forms.Form):
	email=forms.EmailField(required=True)
	password=forms.CharField(widget=forms.PasswordInput)

class SearchMail(forms.Form):
	sender=forms.EmailField(required=True)
	subject=forms.CharField(max_length=20)
	account=forms.CharField(widget=forms.HiddenInput)
	password=forms.CharField(widget=forms.HiddenInput)

class SendTo(forms.Form):
	receiver=forms.EmailField()
	account=forms.CharField(widget=forms.HiddenInput)
	password=forms.CharField(widget=forms.HiddenInput)


