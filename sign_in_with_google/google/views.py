
from django.utils import simplejson
import urllib
import urllib2


from django.shortcuts import render_to_response
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext

from google.models import GoogleProfile
from google.backends import *
from google.workingGmail import scope, consumer
import workingGmail
import xoauth
import email
import email.header
import imaplib
import sys
from django.utils import simplejson
from gmailConnector import gmailConnector

LOGIN_REDIRECT_URL = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

authenticate_url = 'https://accounts.google.com/o/oauth2/auth'
access_token_url = 'https://accounts.google.com/o/oauth2/token'
#access_token_url = 'https://www.google.com/accounts/OAuthGetAccessToken'

scope = 'https://mail.google.com/'
consumer = xoauth.OAuthEntity('anonymous', 'anonymous')
imap_hostname = 'imap.googlemail.com'
DOMAIN = 'http://localhost:8000'

client_id= 'anonymous'
client_secret= 'anonymous'

# How many messages will be fetched for listing?
MAX_FETCH = 50


import config
#class Config():
#        pass
#config = Config()


def google_login(request):

	next = request.GET.get('next', None)
	if next:
		request.session['google_login_next'] = next
	
	url = '%s?client_id=%s&response_type=code&scope=%s&redirect_uri=%s%s' % (authenticate_url, settings.GOOGLE_CLIENT_ID, scope, settings.DOMAIN, reverse('google_callback'))
        #import pdb; pdb.set_trace()
	return HttpResponseRedirect(url)

def google_callback2(request):
 
        #The request token here is used to request the access token below for future use along with the oauthverifier
        req_token = request_token2

        oauth_verifier = request.GET['oauth_verifier'] # retrieved from the verification url
        
        
        access_token = xoauth.GetAccessToken(consumer, req_token, oauth_verifier, config.google_accounts_url_generator)
         
        global xoauth_string  # this actually is suppose be retrieved from the database in the future
        
        xoauth_string = xoauth.GenerateXOauthString(consumer, access_token, config.user, 'IMAP',xoauth_requestor_id=None, nonce=None, timestamp=None)
        sys.stdout = sys.__stdout__

        # Get unread/unseen list
        imap_conn = imaplib.IMAP4_SSL(imap_hostname)
        imap_conn.debug = 4
        imap_conn.authenticate('XOAUTH', lambda x: xoauth_string)
        print "Success"
  
        # Set readonly, so the message won't be set with seen flag
        imap_conn.select('INBOX', readonly=True)
        typ, data = imap_conn.search(None, 'UNSEEN')
        unreads = data[0].split()
        print '%d unread message(s).' % len(unreads)
        ids = ','.join(unreads[:MAX_FETCH])
        if ids:
          typ, data = imap_conn.fetch(ids, '(RFC822.HEADER)')
          for item in data:
            if isinstance(item, tuple):
              raw_msg = item[1]
              msg = email.message_from_string(raw_msg)
              # Some email's header are encoded, for example: '=?UTF-8?B?...'
              print '\033[1;35m%s\033[0m: \033[1;32m%s\033[0m' % (
                  email.header.decode_header(msg['from'])[0][0],
                  email.header.decode_header(msg['subject'])[0][0],
                  )
        #imap_conn.close()
        #imap_conn.logout()

        #connection to Gmail server
        g = gmailConnector()
                
        g.login('XOAUTH', lambda x: xoauth_string)
        g.get_mailboxes()
        print "MY Test"
        # To list all mailboxes
        for item in g.mailboxes:
          print item

        # Returns a count of all emails
        print g.get_mail_count('Inbox')
        
        print "Test complete"

        imap_conn.close()
        imap_conn.logout()
        print "Program Complete"


        return render_to_response('index.html', locals())



## def search_Gmail( g, searchTag, personfrom):
##
##
##        imap_conn = imaplib.IMAP4_SSL(imap_hostname)
##        imap_conn.debug = 4
##        imap_conn.authenticate('XOAUTH', lambda x: xoauth_string)
##        print "Success"
##
##
##        matched = []
##        ml = g.get_mails_from(personfrom)
##        print ml #message ids are printed
##
##        result, data = g.M.uid('search', None, "ALL") # search and return uids instead  
##        latest_email_uid = data[0].split()[-1]  
##        result, data = g.M.uid('fetch', latest_email_uid, '(RFC822)')  
##        raw_email = data[0][1] 
##
##        email_message = email.message_from_string(raw_email)  
##        print email_message['To']  
##        print email.utils.parseaddr(email_message['From']) 
##
##        # this for loop prints the messages that match the search criteria
##        for iter in range(len(ml)):
##            matched.append(g.get_mail_from_id(ml[iter]) )
##            print "From:",personfrom
##            print matched[iter]
##
##        tes = g.find_user_subject(searchTag, folder='Inbox')
##        print tes
##        we = tes[0]
##        we2 = g.get_mail_from_id(we)
##  
##        print we2
##        
##        return (we2,personfrom)
##    #===========================================================================
##    
##    # testing to search for searchTag in the subject     
##  
##    e_output = []
##    e_output = search_criteria(g,searchTag,personfrom)
##    
##    g.logout()
##
##    return e_output













    
def google_callback(request):

    code = request.GET.get('code','')

    config.user = 'mrmekit@gmail.com'  # try to replace this with form input variable get from database
    #config.user = 'waldin@gmail.com'

    config.google_accounts_url_generator = xoauth.GoogleAccountsUrlGenerator(config.user)
     
    request_to = xoauth.GenerateRequestToken(consumer, scope, nonce=None, timestamp=None,google_accounts_url_generator=config.google_accounts_url_generator)
    global request_token2
    request_token2 = request_to[0]
    auth_url = request_to[1] #redirect url to get authorisation form 
   
    redirect_uri = auth_url

    #import pdb; pdb.set_trace()
     
    user = 1
    #import pdb; pdb.set_trace()
    
    if not user:
    	next = reverse('auth_login')
    else:    
        next = request.session.get('google_login_next', None)
    	if next:
        	del request.session['google_login_next']
        else:
        	next = LOGIN_REDIRECT_URL
        
        #login(request, user)
   
    #return render_to_response('index.html', locals())
    #return HttpResponseRedirect(next)
    return HttpResponseRedirect(auth_url)
    
 
