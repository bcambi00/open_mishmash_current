from django.conf.urls.defaults import patterns, include, url
from mish.views import index

handler500 = 'djangotoolbox.errorviews.server_error'

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'openmishmash.views.home', name='home'),
    # url(r'^openmishmash/', include('openmishmash.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
    ('^$','mish.views.index'),
    ('^authorize/','mish.views.registration'),
    ('^google6840dd608b7e27bb.html$','mish.views.google'),
    (r'^channels/select/(trigger|event)/$','mish.views.select'),
    (r'^trigger/service/$','mish.views.service'),
    (r'^start_response/$','mish.views.trigger_out'),
    (r'^response/$','mish.views.criteria'),
    (r'^created/$','mish.views.success'),	

                       
)
