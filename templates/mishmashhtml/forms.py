from django import forms

class Registration(forms.Form):
	username=forms.CharField(max=length=100)
	email=forms.EmailField(required=True)
	password=forms.CharField(widget=forms.PasswordInput)
	confirmpassword=forms.CharField(widget=forms.PasswordInput)
	                      	
    class Meta:
        model = User
        fields = ("username",)

    def clean_confirmpassword(self):
        password = self.cleaned_data.get("password", "")
        confirmpassword = self.cleaned_data["confirmpassword"]
        if password != confirmpassword:
            raise forms.ValidationError("The two password fields didn't match.")
        return confirmpassword

class LoginForm(forms.Form):
	username=forms.CharField(required=True)
	password=forms.CharField(widget=forms.PasswordInput)
	
	
	
	

	    def save(self, commit=True, domain_override=None,
             email_template_name='registration/signup_email.html',
             use_https=False, token_generator=default_token_generator):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email1"]
        user.is_active = False
        if commit:
            user.save()
        if not domain_override:
            current_site = Site.objects.get_current()
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override
        t = loader.get_template(email_template_name)
        c = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'uid': int_to_base36(user.id),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': use_https and 'https' or 'http',
            }
	
	
	
	


	
